let string1 = "Zuitt";
let string2 = "coding";
let string3 = "Bootcamp";
let string4 = "teaches";
let string5 = "javascript";

let sentence = `${string1} ${string2} ${string3} ${string4} ${string5}.`
console.log(sentence);
console.log(Math.pow(5, 3));

//ES6 Updates
	//EcmaScript6 or ES6 is the new version of javascript
	//javascript is formally known as EcmaScript
	//ECMAScript - European Computer Manufacturers Association Script
		//



//exponent operator (**) - ES6 update
let fivePowerOf2 = 5 ** 2;
console.log(fivePowerOf2);

//array destructuring
let array = ["Kobe", "Lebron", "Shaq", "Westbrook"];
console.log(array[2]);

const [kobe, lebron, shaq] = array;
console.log(kobe);
console.log(lebron);
console.log(shaq);

//mini-activity
let array2 = ["Curry", "Lillard", "Paul", "Irving"];
const [pointGuard1, pointGuard2, pointGuard3, pointGuard4] = array2;
console.log(pointGuard1);
console.log(pointGuard2);
console.log(pointGuard3);
console.log(pointGuard4);

let bandMembers = ["Harley", "Zac", "Jeremy", "Taylor"];
const [vocals, lead,, bass] = bandMembers;
console.log(vocals);
console.log(lead);
console.log(bass);
//note: order matters in array destructuring

let person = {
	name: "Jeremy Davis",
	birtday: "January 2, 1996",
	age: 24
}

let sentence3 = `Hi I am ${person.name}`;
console.log(sentence3);

const {age, firstName, birtday} = person;
console.log(age);
	//result: 24
console.log(firstName);
	//result: undefined
console.log(birtday);
	//result: January 2, 1996
//note: order does not matter

let pokemon1 = {
	name: "Charmander",
	level: 11,
	type: "Fire",
	moves: ["Ember", "Scratch", "Leer"]
}

const {name, level, type, moves} = pokemon1;
console.log(`My pokemon is ${name}, it is in level ${level}. It is a ${type} type pokemon. It's moves are ${moves.join(', ')}.`);


//arrow function
const hello = () => {
	console.log('Hello World');
};
hello();

const greet = (person) => {
	console.log(`Hi ${person.name}`)
};
greet(person);

//implicit return
	//allows us to return value without the use of return keyword.
	//implicit return only works with one-line function

const addNum = (num1, num2) => num1 + num2;
let sum = addNum(55,60);
console.log(sum);

//this keyword
let protagonist = {
	name: "Could Strife",
	occupation: "Soldier",
	//traditional method would have this keyword refer to the parent object
	greet: function() {
		console.log(this);
		console.log(`Hi I am ${this.name}`);
	},
	//arrow function refer to the global window
	introduce: () => {
		console.log(this);
		console.log(`I work as ${this.ocupation}`);
	}
}
protagonist.greet();
protagonist.introduce();

//class-base objects blueprints
	//in javascript, classes are templates of objects
	//we can create objects out of the use of classes
	//before the introduction of classes in js, we mimic this behaviour or this ability to create objects out of templates with the use of constructor

function Pokemon(name, type, lvl) {
	this.name = name;
	this.type = type;
	this.lvl = lvl;
};


//ES6 class creation
class Car {
	constructor(brand, name, year) {
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}
let car1 = new Car("Toyota", "Vios", "2002");
console.log(car1);