//activity #1
let student1 = {
	name: "Shawn Michaels",
	birthday: "May 5, 2003",
	age: 18,
	isEnrolled: true,
	classes: ["Philosphy 101", "Social Sciences 201"]
}

let student2 = {
	name: "Steve Austin",
	birthday: "June 15, 2001",
	age: 20,
	isEnrolled: true,
	classes: ["Philosphy 401", "Natural Sciences 402"]
}


const introduce = (student) => {
const {name, birthday, age, isEnrolled, classes} = student2;

	//Note: You can destructure objects inside functions.

	console.log(`Hi! I'm ${name}. I am ${age} years old.`);
	console.log(`I study the following courses: ${classes.join(' and ')}.`);
}
introduce()

const getCube = (num) => console.log(num ** 3);
let cube = getCube(3);

let numArr = [15,16,32,21,21,2]
const [num1, num2, num3, num4, num5, num6] = numArr;

numArr.forEach(num => console.log(num));

let numsSquared = numArr.map(num => num ** 2);

console.log(numsSquared);

//activuty #2
class Dog {
	constructor(name, breed, dogAge) {
		this.name = name;
		this.breed = breed;
		this.dogAge = dogAge * 7;
	}
}

let boyDog = new Dog("Biscuit", "Saint Bernard", 2);
let girlDog = new Dog("Winter", "AsPin", 1);

console.log(boyDog);
console.log(girlDog);